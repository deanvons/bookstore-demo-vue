import Books from '../components/Books/Books'
import BookDetail from '../components/Books/BookDetail'
import VueRouter from 'vue-router'

const routes = [

    { path: '/', component: Books },
    { path: '/:bookId', component: BookDetail }

]

const router = new VueRouter({ routes })

export default router